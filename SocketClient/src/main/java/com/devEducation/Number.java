package com.devEducation;

public class Number {
    private final String value;

    public Number(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
