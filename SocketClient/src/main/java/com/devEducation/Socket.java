package com.devEducation;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.EnumSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javafx.scene.control.TableView;

public class Socket {

    public static final Logger logger = LoggerFactory.getLogger(Socket.class);
    private SocketChannel socketChannel;
    private SocketAddress socketAddress;
    private ByteBuffer byteBuffer;

    private static final int PORT = 9000;
    private static final String HOST = "localhost";

    public void setCommandStart() throws IOException {
        socketChannel = SocketChannel.open();
        socketAddress = new InetSocketAddress(HOST, PORT);
        if (!socketChannel.isConnected()) {
            socketChannel.connect(socketAddress);
            logger.info("Hello from JavaFX");
        }
        String result = "HelloFromFX";
        byteBuffer = ByteBuffer.allocate(1024);
        byteBuffer.put(result.getBytes());
        byteBuffer.flip();
        while (byteBuffer.hasRemaining()) {
            socketChannel.write(byteBuffer);
        }
        byteBuffer.clear();
    }

    public void setCommandGet(String path) throws IOException {
        socketChannel = SocketChannel.open();
        socketAddress = new InetSocketAddress(HOST, PORT);
        if (!socketChannel.isConnected()) {
            socketChannel.connect(socketAddress);
            logger.info("Get from JavaFX");
        }
        String result = "GetFromFX";
        byteBuffer = ByteBuffer.allocate(1024);
        byteBuffer.clear();
        byteBuffer.put(result.getBytes());
        byteBuffer.flip();
        while (byteBuffer.hasRemaining()) {
            socketChannel.write(byteBuffer);
        }

        Path pathFile = Paths.get(path);
        FileChannel fileChannel = FileChannel.open(pathFile, EnumSet.of(StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE));
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        while (socketChannel.read(buffer) > 0) {
            buffer.flip();
            fileChannel.write(buffer);
            buffer.clear();
        }
        fileChannel.close();
        logger.info("Received");
    }

    public void getNumber(TableView<Number> table) {
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        String response;
        while (true) {
            try {
                if (!(socketChannel.read(buffer) > 0)) break;
            } catch (IOException e) {
                logger.error("Something went wrong");
                e.printStackTrace();
            }
            response = new String(buffer.array(), StandardCharsets.UTF_8);
            table.getItems().add(new Number(response));
            logger.info(response);
            buffer.clear();
        }
    }
}