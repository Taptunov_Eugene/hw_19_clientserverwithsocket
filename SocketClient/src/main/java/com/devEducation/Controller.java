package com.devEducation;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    public TableView<Number> table;

    @FXML
    private TableColumn<Number, String> columnNumbers;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        prepareTable();
    }

    @FXML
    void action(ActionEvent event) throws IOException {
        new Thread(() -> {
            try {
                setCommandStart();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    @FXML
    void actionGet(ActionEvent event) throws IOException {
        Platform.runLater(() -> {
            try {
                setCommandGet();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void setCommandStart() throws IOException {
        prepareTable();
        Socket socket = new Socket();
        socket.setCommandStart();
        socket.getNumber(table);
    }

    public void setCommandGet() throws IOException {
        prepareTable();
        Socket socket = new Socket();
        String path = onSave();
        socket.setCommandGet(path);
    }

    private String onSave() {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");
        fileChooser.getExtensionFilters().add(extFilter);
        fileChooser.setInitialFileName("Received");

        File file = fileChooser.showSaveDialog(null);
        System.out.println(file.getAbsolutePath());

        return file.getAbsolutePath();
    }

    private void prepareTable() {
        columnNumbers.setCellValueFactory(new PropertyValueFactory<Number, String>("value"));
    }

}
