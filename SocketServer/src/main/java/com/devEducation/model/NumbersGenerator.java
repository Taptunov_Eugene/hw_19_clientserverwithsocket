package com.devEducation.model;

import com.devEducation.database.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.sql.SQLException;
import java.util.Random;

public class NumbersGenerator {

    private static final Logger logger = LoggerFactory.getLogger(NumbersGenerator.class);

    public void generateNumbers(ByteBuffer byteBuffer, SocketChannel socketChannel, Database mysql) throws SQLException, IOException, InterruptedException {
        Random random = new Random();

        for (int i = 0; i < 100; i++) {
            int numbers = random.nextInt(1000);
            logger.info(String.valueOf(numbers));
            mysql.insertNumbers(numbers);
            String number = String.valueOf(numbers);
            byteBuffer.put(number.getBytes());
            byteBuffer.flip();

            while (byteBuffer.hasRemaining()) {
                socketChannel.write(byteBuffer);
            }
            Thread.sleep(1000);
            byteBuffer.clear();
        }
    }
}
