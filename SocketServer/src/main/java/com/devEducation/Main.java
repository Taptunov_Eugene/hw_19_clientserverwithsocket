package com.devEducation;

import com.devEducation.socket.ServerSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.SQLException;

public class Main {

    public static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws InterruptedException, SQLException, IOException {
        ServerSocket serverSocket = new ServerSocket(9000);
        serverSocket.socketChannelWorking();
        logger.info("We successfully connected to the server");
    }
}
