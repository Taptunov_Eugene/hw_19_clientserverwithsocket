package com.devEducation.socket;

import com.devEducation.database.Database;
import com.devEducation.model.NumbersGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;

public class ServerSocket {

    private static final Logger logger = LoggerFactory.getLogger(ServerSocket.class);
    private final ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
    private ServerSocketChannel serverSocketChannel;
    SocketChannel socketChannel = null;
    Database mysql = null;

    public ServerSocket(int port) {
        try {
            serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.socket().bind(new InetSocketAddress(port));
            logger.info("Connection was successful");
        } catch (IOException e) {
            logger.error("Failed to make connection!");
            e.printStackTrace();
        }
    }

    public SocketChannel accept() throws IOException {
        return serverSocketChannel.accept();
    }

    public ByteBuffer getByteBuffer() {
        return this.byteBuffer;
    }

    public void socketChannelWorking() throws SQLException, IOException, InterruptedException {
        while (true) {
            try {
                socketChannel = accept();
            } catch (IOException e) {
                e.printStackTrace();
            }
            ByteBuffer byteBuffer = getByteBuffer();
            String response = "";

            if (socketChannel.read(byteBuffer) > 0) {
                response = new String(byteBuffer.array(), StandardCharsets.UTF_8);
                byteBuffer.clear();
                if (response.startsWith("H")) {
                    NumbersGenerator numbersGenerator = new NumbersGenerator();
                    mysql = new Database();
                    numbersGenerator.generateNumbers(byteBuffer, socketChannel, mysql);
                }
                if (response.startsWith("G")) {
                    String numbersCSV = mysql.getNumbers();
                    byteBuffer.put(numbersCSV.getBytes());
                    byteBuffer.flip();
                    while (byteBuffer.hasRemaining()) {
                        socketChannel.write(byteBuffer);
                        String checking = new String(byteBuffer.array(), StandardCharsets.UTF_8);
                        logger.info(checking);
                    }
                }
            }
        }
    }
}
